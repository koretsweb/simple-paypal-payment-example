<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 26.03.17
 * Time: 12:24
 */

namespace app\controllers;

use Yii;
use app\models\PayPal;
use yii\web\Controller;

class IndexController extends Controller
{
    public function actionIndex()
    {
        $model = new PayPal();
        $response = null;
        if ( $model->load(Yii::$app->request->post()) && $model->validate() ) {
            
            $user      = Yii::$app->params['paypalAcc']['user'];
            $password  = Yii::$app->params['paypalAcc']['pwd'];
            $signature = Yii::$app->params['paypalAcc']['signature'];
            $version   = Yii::$app->params['paypalAcc']['version'];
            $baseURI   = Yii::$app->params['baseURL'];

            $response = self::expressCheckoutQuery($user, $password, $signature, $version, $model->amount, $model->description,
                $baseURI . 'success', $baseURI . '/error');




        }
        return $this->render('index', compact('model', 'response'));
    }

    /**
     * @see https://developer.paypal.com/docs/classic/api/merchant/SetExpressCheckout_API_Operation_NVP
     * @return bool|mixed
     */
    public static function expressCheckoutQuery($user, $password, $signature, $version = 200, $amount, $desc = null,
                                                $returnUrl, $cancelUrl)
    {
        $api_request = 'USER=' . urlencode( $user )
            .  '&PWD=' . urlencode( $password )
            .  '&SIGNATURE=' . urlencode( $signature )
            .  '&VERSION=' . urlencode( $version )
            .  '&METHOD=SetExpressCheckout'
            .  '&AMT=' . urlencode($amount)
            .  '&DESC=' . urlencode($desc)
            .  '&RETURNURL' . urlencode($returnUrl)
            .  '&CANCELURL' . urlencode($cancelUrl);


        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp' );

        curl_setopt( $ch, CURLOPT_VERBOSE, 1 );

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        // Set the API parameters for this transaction
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $api_request );

        // Request response from PayPal
        $response = curl_exec( $ch );

        curl_close( $ch );

        // If no response was received from PayPal there is no point parsing the response
        if( !$response ){
            return false;
        }

        return $response;
    }
}