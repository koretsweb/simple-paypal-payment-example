<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\PayPal */
/* @var $response */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'PayPal payment';
?>
<div class="paypal-payment">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to pay:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'payment-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'amount')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'currency')->dropDownList([
        'USD' => 'USD',
        'UAH' => 'UAH',
        'RUB' =>'RUB'
    ]);
    ?>


    <?= $form->field($model, 'description')->textInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Send', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end();
    if ($response) {
        print_r($response);
    }
    ?>
</div>