<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 26.03.17
 * Time: 12:41
 */

namespace app\models;


use yii\base\Model;

class PayPal extends Model
{

    public $amount;
    public $currency;
    public $description;

    public function rules()
    {
        return [
            [['amount', 'currency', 'description'], 'required'],
            ['amount', 'number'],
            ['description', 'string', 'max' => 127],
            ['currency', 'string', 'length' => 3],
        ];
    }

}